import { Routes} from '@angular/router';
import { LoginPageComponent } from '../components/login/login-modal';
import { AuthGuard } from '../services/auth/authGuard';
import { MainComponent } from '../containers/main/main.container';
import { CanDeactivateGuard } from '../services/auth/authDeactivateGurad';
import { TestAComponent } from '../components/testA';
import { TestBComponent } from '../components/testB';

export const SAMPLE_APP_ROUTES: Routes = [{
  path: 'login',
  component: LoginPageComponent
} , {
  path: 'main',
  component: MainComponent,
  children: [
    { path: '', component: TestAComponent },
    { path: 'test', component: TestBComponent },
  ],
  canActivate: [AuthGuard],
  canDeactivate: [CanDeactivateGuard]
}, {
  path: '',
  redirectTo: '/main',
  pathMatch: 'full'
}, {
  path: '**',
  redirectTo: '/main',
  pathMatch: 'full'
}
];
