import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import { ApiHttp } from '../http/http-api.service';
@Injectable()
export class AuthService {
  LoggedIn: boolean = false;
  // store the URL so we can redirect after logging in
  baseUrl: string = 'https://mk.reamis.net/server.php';
  redirectUrl: string;
  constructor (private http: Http, private apiHttp: ApiHttp) {}

  login(login, pass) {
    let body =
      'module=Base_User&' +
      'action=doLogin&' +
      'userName=' + login +
      '&userPassword=' + pass;

    return this.apiHttp.post('', body)
      .map(res => Object.assign(res, {_body: this.parseBadJson(res.text())}))
      .map(res => res.json());
  }
  parseBadJson(str: any): any {
    let jsontemp = str.replace((/([\w]+)(:)/g), '\"$1\"$2');
    return jsontemp.replace((/'/g), '\"');
  }
  logout() {
    this.LoggedIn = false;
  }

  isLoggedIn() {
    return this.LoggedIn;
  }
}
