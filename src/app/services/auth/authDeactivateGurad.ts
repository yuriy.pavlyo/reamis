import { Injectable }    from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable }    from 'rxjs/Observable';
import { AuthService } from './authService';
export interface CanComponentDeactivate {
  canDeactivate: () => boolean | Observable<boolean>;
}
@Injectable()
export class CanDeactivateGuard implements CanDeactivate<CanComponentDeactivate> {
  constructor(private auth: AuthService) {}
  canDeactivate(component: CanComponentDeactivate):
  Observable<boolean> | Promise<boolean> | boolean {
    return !this.auth.isLoggedIn();
  }
}
