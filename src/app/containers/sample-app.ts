import { Component, ViewEncapsulation, ApplicationRef } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { AsyncPipe } from '@angular/common';
import { NgRedux, select } from 'ng2-redux';

import { IAppState } from '../reducers';
import { SessionActions } from '../actions/session';
import rootReducer from '../reducers';
import { middleware, enhancers } from '../store';


@Component({
  selector: 'app',
  directives: [
    ROUTER_DIRECTIVES],
  pipes: [ AsyncPipe ],
  // Allow app to define global styles.
  encapsulation: ViewEncapsulation.None,
  styles: [`../../assets/styles/index.css`,
            `../../assets/styles/main.scss`],
  template: require('./sample-app.html')
})
export class RioSampleAppComponent {

  constructor(
    private ngRedux: NgRedux<IAppState>,
    private actions: SessionActions) {

    ngRedux.configureStore(rootReducer, {}, middleware, enhancers);

  }
};
