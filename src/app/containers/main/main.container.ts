import { Component, ViewEncapsulation, ApplicationRef } from '@angular/core';
import { AsyncPipe } from '@angular/common';

import { mainHtml } from './main.html';
import { ROUTER_DIRECTIVES } from '@angular/router';

@Component({
  selector: 'main-app',
  directives: [ROUTER_DIRECTIVES],
  pipes: [ AsyncPipe ],
  // Allow app to define global styles.
  encapsulation: ViewEncapsulation.None,
  styles: [ ],
  template: mainHtml
})
export class MainComponent {
  constructor() {
    }
};
