export const mainHtml = `

<md-toolbar class="main-toolbar">
    <div class="logo-holder">
        <a routerLink="/main"><img src="src/assets/images/logo.png"></a>
    </div>
    <!-- menu opens when trigger button is clicked -->
    <div class="right-nav-holder" layout="row" layout-align="center center">
        <button md-button aria-label="Imports">Imports</button>
        <button md-button aria-label="Partners">Partners</button>
        <div class="v-divider"></div>
        <md-menu x-position="before" y-position="below">
            <span class="user-pic"><img src="src/assets/images/user.png"></span>
            <!-- Trigger button -->
            <button md-button aria-label="User">
                <span class="username">Marco Kundert</span>
                <i class="material-icons dropdown-arrow">keyboard_arrow_down</i>
            </button>
            <md-menu-content>
                <button md-button>Test Link 1</button>
                <button md-button>Test Link 2</button>
            </md-menu-content>
        </md-menu>
    </div>
</md-toolbar>
<md-toolbar class="main-nav md-whiteframe-z2">
    <nav>
        <ul layout="row" layout-align="start center" class="list-inline">
            <li><a routerLink="/"><i class="icon-financial"></i>FINANCIALS</a></li>
            <!--ADD  routerLinkActive="active" Later for proper styling -->
            <li><a routerLink="/"><i class="icon-reports"></i>REPORTS</a></li>
            <li><a routerLink="/dashboard"  routerLinkActive="active"><i class="icon-dashboard"></i>Dashboard</a></li>
            <li ><a routerLink="/assets"  routerLinkActive="active"><i class="icon-assets"></i>Assets</a></li>
            <li><a routerLink="/"><i class="icon-analytics"></i>ANALYTICS</a></li>
            <li><a routerLink="/"><i class="icon-strategy"></i>STRATEGY</a></li>
            <li><a routerLink="/"><i class="icon-invest"></i>INVEST</a></li>
        </ul>
    </nav>
</md-toolbar>
 <router-outlet></router-outlet>
`;
