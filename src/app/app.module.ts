import {HttpModule, BrowserXhr} from '@angular/http';
import { NgRedux } from 'ng2-redux';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { provideRouter } from '@angular/router';
import { SAMPLE_APP_ROUTES } from './routes/sample-app';
import { BrowserModule  } from '@angular/platform-browser';
import { RioSampleAppComponent } from './containers/sample-app';
import { SessionActions } from './actions/session';
import { AuthGuard } from './services/auth/authGuard';
import { AuthService } from './services/auth/authService';
import { API_HTTP_PROVIDERS } from './services/http/http-api.service';
import { CanDeactivateGuard } from './services/auth/authDeactivateGurad';
import {MATERIAL_DIRECTIVES, MATERIAL_PROVIDERS} from 'ng2-material';
@NgModule({
      declarations: [RioSampleAppComponent],
      imports: [
          CommonModule,
          BrowserModule,
          HttpModule,
      ],
      providers: [
        API_HTTP_PROVIDERS,
        NgRedux,
        SessionActions,
        AuthGuard,
        CanDeactivateGuard,
        AuthService,
        provideRouter(SAMPLE_APP_ROUTES),
        MATERIAL_PROVIDERS
      ],
      bootstrap: [RioSampleAppComponent]
  })
export class AppModule {}
