import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'testA',
  directives: [ ],
  template: `testA  <a routerLink="/main/test" routerLinkActive="active">next</a>`
})
export class TestAComponent {
  constructor(private router: Router) {
  }
  next() {
    this.router.navigate(['/main/test']);
  }
};
