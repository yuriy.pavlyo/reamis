import { Component, EventEmitter, Input, Output } from '@angular/core';


import { htmlLogin } from './login.html';
import { AuthService } from '../../services/auth/authService';
import { Router } from '@angular/router';

@Component({
  selector: 'login-modal',
  directives: [ ],
  template: htmlLogin
})
export class LoginPageComponent {
  private username: string;
  private pass: string;
  private error: any;
  constructor(private auth: AuthService, private router: Router) {}
  login(login, pass) {
    this.auth.login(login, pass)
      .subscribe(res => {
        if (res.success) {
          this.auth.LoggedIn = true;
          this.router.navigate(['/main']);
        } else {
          this.error = Object.assign({}, {error: true, msg: res.errors.reason});
        }
      });
  }
};
