// noinspection TsLint
export const  htmlLogin = `
  <div class="teaser-bg" layout="column" flex="100" layout-align="center center">
        <div class="teaser-form-holder md-whiteframe-1dp">
            <div class="teaser-form-header" layout="row" flex="100" layout-align="center center" layout-padding>
                <img src="src/assets/images/logo.png" alt="" layout-margin>
            </div>
            <md-divider></md-divider>
            <div class="teaser-form-content" layout="column" flex="100" layout-padding>
                <div class="teaser-header" layout-align="center center" flex="100">
                    <h2>Welcome to Reamis<sup>&copy;</sup></h2>
                    <p>Bitte loggen Sie sich ein, Ihr Portfolio wartet</p>
                </div>
                    <div layout-align="center center">
                        <div class="input-holder" layout-align="center center" layout-gt-sm="row">
                            <md-icon class="username"><i class="material-icons">email</i></md-icon>
                            <div class="group" flex="100">
                                <input type="text" (click)="error = undefined" (change)="username = $event.target.value" required>
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Username</label>
                            </div>
                        </div>
                        <div class="input-holder" layout-align="center center" layout-gt-sm="row">
                            <md-icon class="password"><i class="material-icons">lock</i></md-icon>
                            <div class="group" flex="100">
                                <input type="password" (click)="error = undefined" (change)="pass = $event.target.value" required>
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Password</label>
                            </div>
                        </div>
                    </div>
                    <div class="btn-holder" layout-align="end center" layout-gt-sm="row">
                        <button (click)="login(username, pass)" md-raised-button class="md-raised md-primary">Anmeldung</button>
                    </div>
                    <div *ngIf="error">{{error.msg}}</div>
            </div>
        </div>
    </div>
`;
