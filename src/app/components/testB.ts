import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'testA',
  directives: [ ],
  template: `testB  <a routerLink="/main" routerLinkActive="active">prev</a>`
})
export class TestBComponent {
  constructor() {
  }
};
